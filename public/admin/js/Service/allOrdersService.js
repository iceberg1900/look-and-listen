﻿app.service('OrdersService', function ($resource) {
    //$resource('/api/orders');
    var provider = $resource('/api/orders');
    this.getAll = function (filter, success, error) {
        return provider.query(filter, success, error);
    }

});