﻿app.service('UserService', function ($resource) {
    //$resource('/api/orders');
    var provider = $resource('/api/user/:path', { path: '@path' }, { login: { method: 'POST' } });
    this.getUser = function (success, error) {
        return provider.get({}, success, error);
    }
    //login
    this.login = function (user, success, error) {
        user.path = 'login';
        provider.login(user, success, error);//<--методи
    };
});