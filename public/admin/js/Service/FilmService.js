﻿app.service("FilmService", function ($resource) { //resource- це фабрика яка навчає мавчативхідний параметр, this-вказує на обект самої функції не можливо доступитися щоб змінити юзерам зис дає таку видимість.
    var $provider = $resource("/api/films/:id", { id: '@id' }, {
        update: { method: 'PUT' }
    });

    this.getAll = function (filter, success, error) {
        $provider.get(filter, success, error);
    };

    this.get = function (id, success, error) {
        $provider.get({ id: id }, success, error);//wraper над методом get
        //багато запитів query
        //один запит-get
    };
    this.add = function (film, succsess, error) {
        $provider.save(film, succsess, error);//<--методи
    };
    this.remove = function (film, succsess, error) {
        $provider.delete(film, succsess, error);//<--методи
    };
    this.update = function (film, succsess, error) {
        $provider.update(film, succsess, error);//<--методи
    };
});
//FilmService.getAll()
//FilmService.getAll(1)
//FilmService.add({ title: "title" })
//FilmService.update({ id: 1, title: "title" })!обовязково id 1парметр
//FilmService.remove({ id: 1 })
