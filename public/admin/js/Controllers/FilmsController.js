﻿app.controller('FilmsController',
    function ($scope, FilmService, $rootScope) {
        FilmService.getAll({},//this.getAll(filter,success,error);
            function (films) { //-SUCCESS
                $scope.films = films.items;
                $scope.filter = {
                    total: films.total,
                    page: films.page,
                    size: films.size
                }
            }, function (reason) {
                alert('Error'); //->ERROR
                console.log(reason);
            });

        $scope.pageChanged = function () {
            FilmService.getAll($scope.filter,
              function (films) {
                  $scope.films = films.items;
                  $scope.filter = {
                      total: films.total,
                      page: films.page,
                      size: films.size
                  }
              }, function (reason) {
                  alert('Error');
                  console.log(reason);
              });
        }

        $scope.inverse = false;
        $scope.remove = function (film) {
            var removeFunction = function () {
                var index = $scope.films.map(function (f) { //mapped-перетворювати. створює новий масив id
                    return f.id;
                }).indexOf(film.id);
                FilmService.remove(film,
                    function () {
                        $scope.films.splice(index, 1);//$rootScope.films.splice(index, 1); параметр після коми вказує на кількість елементів для видалення.
                    }, function (reason) {
                        alert(reason.message);
                        console.log(reason);
                    });

            };
            $rootScope.$broadcast("confirm-window:open", {
                title: "",
                message: "",
                confirmFunction: removeFunction
            });
        };
        $scope.changeSort = function () {
            $scope.inverse = !$scope.inverse; //true or false; 
        };
    });
