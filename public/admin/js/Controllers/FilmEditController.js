﻿app.controller('FilmEditController', function ($scope, FilmService, $routeParams, $location, $resource) {
    $scope.id = $routeParams.id;
    genreResource = $resource('/api/genres');
    $scope.loadGenres = function (query) {
        return genreResource.query({ searchTerm: query }).$promise;// обект який виконується одразу but повертає результат success or error потім.

    };

    if ($scope.id) {
        FilmService.get($scope.id,
            function (film) {
                $scope.film = film;
            }, function (reason) {
                alert(reason.data.message);
                console.log(reason);
                $location.path('/films');

            });
    }
    $scope.$watch('film.title', function () {
        if ($scope.film)
        $scope.film.alias = $scope.film.title.toLowerCase().replace(/\s/g, '-');//пробіли
    });

    $scope.save = function () {
        FilmService.update($scope.film,
            //success
            function () {
                $location.path('/films');
            },
            //error
            function (reason) {
                alert(reason.data.message);
                console.log(reason);
            });
    };

    $scope.addNew = function () {
        FilmService.add($scope.film,
            function () {
                $location.path('/films');
            },
            //error
            function (reason) {
                alert(reason.data.message);
                console.log(reason);
            });
    };
    //    $scope.watch(film.title, function({
    //if($scope.film)
    //      $scope.film.alias=$scope.film.title.replace(/\S/g,'-' ).toLowerCase()
//    })
});

