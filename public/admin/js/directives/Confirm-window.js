﻿app.directive('confirmWindow', function () {
    /*app - обєкт ангуларівської аплікації який ми створили за допомогою функції angular.module
    те що нам повернули ми зберігли собі в обєкт app і зараз використовуємо аби додати нову   
    директиву, ми самі придумали йому назву 'confirmWindow' за цією назвою ми можемо його використати 
    як ми це зробили на ‘index.html’ за допомогою елементу <confirm-window></confirm-window> */
    return {
        templateUrl: 'templates/confirm-window.html',
        /*templateUrl - параметр яким ми вказуємо де лежить шаблон нашого вікна
          шлях потрібно вказувати відносно того де лежить index.html
          index.html - основний файл на якому і виконується вся логіка нашої апплікації
          таким чином виходить що confirm-window.html лежить в папці templates 
          папка templates лежить там само де і наш файл index.html
        */
        link: function ($scope, $element, $attrs) {
            /*link - це логіка нашої директиви, те що вміє робити наша директива */
            $scope.$on('confirm-window:open', function (event, params) {
                $scope.title = params.title;
                $scope.message = params.message;
                $scope.toDo = params.confirmFunction;
                $('.modal', $element).modal();
            });
            /* що ж вона вміє?
               вона вміє показати віконечко з повідомленням та заголовком і виконати функцію яку 
               потрібно виконати якщо людина погоджується
               
               це вікно можна використати з будь якого місця нашого додатку
               для цього потрібно написати $rootScope.$broadcast('confirm-window:open', ...
               і передати відповідний обєкт (для прикладу подивіться “testController.js”)  
               обєкт який ми хочемо отримати повинен мати поля title, message, confirmFunction
          
                      тобто:
                      наша дириктива привязалася на подію 'confirm-window:open' 
                   як вона це зробила?
                   $scope.$on('confirm-window:open', function (event, params) {
                      event - обєкт який містить інформацю яка саме подія згенерувалась, в даному випадку 
          'confirm-window:open'
               params - обєкт який якраз містить потрібні нам дані title, message, confirmFunction
               в середині функціїї ми зберігаємо дані в наш scope і виконуємо бутстрапівську функцію яка 
               показує модальне вікно
              function (event, params) {
                          $scope.title = params.title;
                          $scope.message = params.message;
                          $scope.toDo = params.confirmFunction;
                          
                   $element.find('.modal').modal(); //показуємо модальне вікно
                      });*/
        }
    };
});

