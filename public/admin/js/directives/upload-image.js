﻿/// <reference path="../../Templates/upload-image.html" />
/// <reference path="../../Templates/upload-image.html" />
app.directive("uploadImage", function () {
    return {
        templateUrl: 'templates/upload-image.html',
        scope: {
            file: "=fileModel",

        },
        link: function (scope, element, attrs) {
            scope.progress = 0;
            scope.cancel = function () {
                scope.file = null;
            };
            scope.upload = function () {
                var blob = dataURItoBlob(scope.file.dataUrl);
                var fd = new FormData(document.forms[0]);
                fd.append("image", blob);
                var xhr = new XMLHttpRequest();
                xhr.addEventListener("load", loadComplete);
                xhr.addEventListener("error", uploadFailed);
                xhr.addEventListener("abort", uploadCanceled);
                xhr.upload.addEventListener("progress", uploadProgress);
                xhr.open("POST", "/api/file/upload");
                xhr.send(fd);
            };
            var loadComplete = function () {
                response = JSON.parse(this.response);
                scope.$apply(function () {
                    scope.$parent.setResult(response);
                });
            };
            var uploadProgress = function (event) {
                scope.$evalAsync(function () { //
                    scope.progress = (event.loaded / event.total * 100);
                });
            };
            var uploadFailed = function () {
                console.log(arguments);
            };
            var uploadCanceled = function () {
                console.log(arguments);
            };
            function dataURItoBlob(dataURI) {
                // convert base64/URLEncoded data component to raw binary data held in a string
                var byteString;
                if (dataURI.split(',')[0].indexOf('base64') >= 0)
                    byteString = atob(dataURI.split(',')[1]);
                else
                    byteString = unescape(dataURI.split(',')[1]);

                // separate out the mime component
                var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                // write the bytes of the string to a typed array
                var ia = new Uint8Array(byteString.length);
                for (var i = 0; i < byteString.length; i++) {
                    ia[i] = byteString.charCodeAt(i);
                }
                return new Blob([ia], { type: mimeString });
            }
        }
    };

});