﻿/// <reference path="../../templates/edit-image.html" />
//app.directive('editImage', function () {
//    return {
//        templateUrl: 'templates/edit-image.html',
//        link: function (scope, element, attrs) {
//            scope.$on('edit-image:edit',
//                function (event, params) {
//                    scope.$apply(function () {
//                        scope.dataUrl = params.dataUrl;
//                    });
//                    element.find('.modal').modal();
//                });
//        }
//    }
//});
app.directive('editImage', function ($rootScope) {
    return {
        templateUrl: 'templates/edit-image.html',
        scope: { type: "@" },//@ copy all object
        link: function (scope, element, attrs) {
            console.log("type", scope.type);
            scope.canvas = $('<canvas>')[0];
            scope.window = $('.modal', element);

            scope.option = { bgColor: 'black' };
            if (scope.type == "poster") {
                scope.option.height = 500;
                scope.option.width = 300,
                scope.option.setSelect = [190, 0, 700, 700],
                scope.option.aspectRatio = 300 / 500;
            }
            else {
                scope.option.height = 500;
                scope.option.width = 1200,
                scope.option.setSelect = [190, 0, 700, 700],
                scope.option.aspectRatio = 1200 / 500;
            }

            scope.image = $('#edit-img', element);
            scope.image.on('load', function () {//НАГАДАТИ ЖЕНІ!!!!
                if ($(this).data('Jcrop'))
                    $(this).data('Jcrop').destroy();
                $(this).Jcrop(scope.option);
                //    width: 300, //$(this).attr('id')
                //    height: 500,
                //    aspectRatio: 3 / 5,
                //    setSelect: [190, 0, 700, 700],
                //    bgColor: 'black',
                //    minSize: 500
            });


            scope.$on('edit-image:edit', function (event, params) {
                scope.$apply(function () {
                    scope.dataUrl = params.dataUrl;
                    scope.window.modal();
                });
            });
            //scope.close = function () {
            //    scope.image.data('Jcrop').destroy();
            //};
            scope.save = function () {
                var scale = scope.image[0].naturalWidth / scope.image.width();
                var coords = scope.image.data('Jcrop').tellSelect();
                scope.canvas.width = scope.option.width;
                scope.canvas.height = scope.option.height;
                var context = scope.canvas.getContext('2d');

                context.drawImage(scope.image[0],
                    coords.x * scale, coords.y * scale,
                    coords.w * scale, coords.h * scale,
                    0, 0,
                    scope.canvas.width, scope.canvas.height);
                var resImage = scope.canvas.toDataURL("image/png", 0.6);
                scope.$parent.$broadcast('edit-image:save', {
                    dataUrl: resImage
                });
                scope.image.data('Jcrop').destroy();
            };
        }
    };
});


//jcrop_api.setOptions(
//{
//    aspectRatio: 2 / 1
//});