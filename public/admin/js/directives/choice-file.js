﻿//directive choiseFile
//це якраз і є вашим домашнім завданням
//завдання наступне:
//як тільки користувач вибере файл ви маєте його відмалювати
//при цьому поле для вибору файла має бути заховане
//користувач має право стерти файл (чи закрити)
//при цьому поле для вибору файла має бути показане
app.directive('choiseFile', function ($rootScope) {
    return {
        scope: {
            fileResult: "=fileResult",
            type: "@"
            //declare isolate choiseFile scope 
            //це не сам scope а лише опис як би він мав виглядати
            //тут описують дані які хочуть прийняти від користувача дирикторії
            //в якості вхідних параметрів
        },
        templateUrl: 'templates/choise-file.html',
        link: function ($scope, $element, $attr) {
            //FileList - масив обєктів File
            $scope.files = [];
            $scope.image = '';
            $scope.inputFile = function () {
                $('[type=file]', $element).trigger('click');
            };
            $scope.setResult = function (result) {
                $scope.fileResult = result;
            };
            //$scope.$watch слідкує за змінами 
            //і коли такі зміни відбудуться викликає функцію 
            //яку ви передали в якості другого параметру 
            //            $scope.$watch('files', function (newValue, oldValue) {
            //                console.log($scope.files);
            //                if ($scope.files && $scope.files.length) {
            //                    var reader = new FileReader();
            //                    reader.onload = function (event) {
            //                        $rootScope.$broadcast('edit-image:edit', {
            //                            dataUrl:event.target.result
            //                        });
            //                        $scope.$apply(function () {
            //                            $scope.image=event.target.result;//event.target-це наш рідер.
            //                        });
            //                    }
            //                    reader.readAsDataURL($scope.files[0]);//що повертає браузер.
            //                }
            //            });

            //apply-це відстрокування,зробити коли буде можливість.
            $scope.$watch('files', function (newValue, oldValue) {
                if ($scope.files && $scope.files.length) {//scope.files масив файлів
                    $scope.image = $scope.files[0];
                    $scope.file = [];
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        $scope.$broadcast('edit-image:edit', {
                            dataUrl: event.target.result
                        });
                        $scope.$broadcast("files-model:clear");
                    };
                    reader.readAsDataURL($scope.image);
                }
            });
            $scope.$on('edit-image:save', function (event, params) {
                console.log(params.dataUrl);
                $scope.image.dataUrl = params.dataUrl;
            });
            var dropbox = $($element).find(".dropbox").get(0);//знаходимо елемент з класом dropbox get-для того щоб доступитися до елементу який знаходться в врапері евент лістенер

            // init event handlers

            dropbox.addEventListener("dragenter", function (evt) { //коли ми відпустили файлик
                evt.stopPropagation();
                evt.preventDefault();
                $scope.$evalAsync(function () {
                    $scope.dropClass = 'someClass';
                });
            }, false);//привязуемся на події
            dropbox.addEventListener("dragleave", function (evt) { //одна функція для обробки 2 подій
                evt.stopPropagation();
                evt.preventDefault();
                $scope.$apply(function () { //remove class
                    $scope.dropClass = '';
                });
            }, false);
            dropbox.addEventListener("dragover", function (evt) { //коли ми не відпустили файлик
                evt.stopPropagation();
                evt.preventDefault();
            }, false);
            dropbox.addEventListener("drop", function (evt) {
                //console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)))
                evt.stopPropagation();
                evt.preventDefault();
                if (evt.dataTransfer && evt.dataTransfer.files && evt.dataTransfer.files.length) {
                    if (isImage(evt.dataTransfer.files[0].name)) {
                        $scope.$apply(function () {
                            $scope.files = evt.dataTransfer.files;
                            $scope.dropClass = '';
                        });
                    }
                }
            }, false);
            function getExtension(filename) {
                var parts = filename.split('.');
                return parts[parts.length - 1];
            }

            function isImage(filename) {
                var ext = getExtension(filename);
                switch (ext.toLowerCase()) {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                        //etc
                        return true;
                }
                return false;
            }
        }
    };
})
 .directive("filesModel", [function () {
     return {
         scope: {
             model: "=filesModel"
         },
         link: function ($scope, $element, $attrs) {
             $element.bind("change", function (event) {
                 $scope.$apply(function () { //виконує застосування змін на певний термін.
                     $scope.model = event.target.files;

                 });

             });
             $scope.$on("files-model:clear", function () {
                 $($element).wrap("<form>").closest("form").get(0).reset();
                 $($element).unwrap();
             });
         }
     };
 }]);


//var dropbox = $element.find(".dropbox")//знаходимо елемент з класом дропбокс


//            // init event handlers
//            function dragEnterLeave(evt) {
//                evt.stopPropagation()
//                evt.preventDefault()
//                $scope.$apply(function () { //remove class
//                    $scope.dropText = 'Drop files here...'
//                    $scope.dropClass = ''
//                })
//            }
//            dropbox.on("dragenter", dragEnterLeave)
//            dropbox.on("dragleave", dragEnterLeave)
//            dropbox.on("dragover", function (evt) {
//                evt.stopPropagation()
//                evt.preventDefault()

//                console.log(evt)//set class

//            })
//            dropbox.on("drop", function (evt) {
//                console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)))
//                evt.stopPropagation()
//                evt.preventDefault()

//                if (evt.dataTransfer.files.length > 0) {
//                    $scope.$apply(function () {
//                        $scope.files = evt.dataTransfer.files
//                    })
//                }
//            })
//        }
//    }
//})






//directive fileModel
//для чого вона?
//вона потрібна оскільки ngModel не працює з input[type=file]
//що вона робить?
//$element.bind("change" ...
//це щось на зразок того що ви колись писали на jQuery
//$('[type=file]').on('change' ...
//тобто функція яка була передана в bind change
//буде викликатись кожного разу як файл був вибраний
//після чого вона ручками міняє модель
//$scope.model = event.target.files;
//коли модель була змінена а це той самий обєкт що і в choiseFile 
//спрацює функція $scope.$watch('files' ... що знаходиться в choiseFile directive
