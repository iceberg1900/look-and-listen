﻿var app = angular.module("App", ['ng', 'ngRoute', 'ngResource', 'ngTagsInput', 'ui.tinymce', 'ngSanitize', 'ui.bootstrap', 'http-auth-interceptor']);
app.config([
    '$routeProvider', function ($routeProvider) {
        $routeProvider.when(
            '/', {
                // має співпадати з назвою після слешу в індексі
                //templateUrl: 'Templates/admin.html',
                controller: 'AdminController'
                })
            .when('/films', {
                templateUrl: 'Templates/films.html',
                controller: 'FilmsController'
            })
            .when('/film/new', {
                templateUrl: 'Templates/filmEdit.html',
                controller: 'FilmEditController'
            })
            .when('/film/:id', {
                templateUrl: 'Templates/filmEdit.html',
                controller: 'FilmEditController'
            })
              .when(
                '/allOrders', {// має співпадати з назвою після слешу в індексі
                    templateUrl: 'Templates/allOrders.html',
                    controller: 'allOrdersController'
                })
            .otherwise({
                redirectTo: '/'
            });
    }
]);

