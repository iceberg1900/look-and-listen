﻿var app = angular.module("App", ['ng', 'ngRoute', 'ngResource', 'ngCookies', 'ngSanitize', 'google.places', 'ui.bootstrap', 'http-auth-interceptor']);//,'ui.bootstrap'ngRoute підключали окремо він не йде по замовчуванні в ядрі angular
app.config([
     '$routeProvider',
    function ($routeProvider) {
        $routeProvider
            .when(
                '/', {// має співпадати з назвою після слешу в індексі
                    templateUrl: 'Templates/Films.html',
                    controller: 'PublicFilmsController'
                })
             .when(
                '/films', {// має співпадати з назвою після слешу в індексі
                    templateUrl: 'Templates/Films.html',
                    controller: 'PublicFilmsController'
                })
            .when(
                '/film', {
                    templateUrl: 'Templates/Film.html',
                    controller: 'PublicFilmController'
                })
            .when(
                '/order', {
                    templateUrl: 'Templates/PublicOrder.html',
                    controller: 'CartController'
                })
            .otherwise({
                redirectTo: '/'
            });
    }

]);
app.filter('totalPrice', function () {
    return function (films) {
        var count = 0;
        if (films && films.length)
            films.forEach(function (film) {
                count += film.count * film.film.price;
            });
        return count;
    };
});

//app.factory('myHttpResponseInterceptor', ['$q', '$location', function ($q, $location,$rootScope) {
//    return {
//        response: function (response) {
//            return promise.then(
//              function success(response) {
//                  return response;
//              },
//            function error(response) {
//                if (response.status === 401) {
//                    $rootScope.broadcast('no-authorize');
//                }
//                    return $q.reject(response);
//            });
//        }
//    }
//}]);
////Http Intercpetor to check auth failures for xhr requests
//app.config(['$httpProvider', function ($httpProvider) {
//    $httpProvider.interceptors.push('myHttpResponseInterceptor');
//}]);

//app.filter('total', function () {
//    return function (films) {
//        var total = 0;
//        films.forEach(function (item) {
//            total += item.count * item.film.price;
//        });
//        return total;
//    };
//});
//app.controller("TestController", function($scope) {
//    $scope.data = 'my message';
//    $scope.message = "";//Hello pepole
//    $scope.titles = ['first', 'second'];
//    $scope.newTitle = "";
//    $scope.string = "new sensei";
//    $scope.current = 1285.500;
//    $scope.some = -128585;
//    $scope.item = 'people';
//    $scope.list = [{
//        'name': 'Julia',
//        'surname': 12,
//        'birthday': '17.03',
//    }, {
//        'name': 'Ira',
//        'surname': 100,
//        'birthday': '21.12',
//    }, {
//        'name': 'Jack',
//        'surname': 1,
//        'birthday': '05.07',
//}];

//    $scope.addNew = function () {
//        if ($scope.newTitle != "") {
//            $scope.titles.push($scope.newTitle),
//            $scope.newTitle = "";
//        }
//    };
//    $scope.remove = function (index) {
//        $scope.titles.splice(index,1);

//    };
//});
//scope-обмеження контроллера
