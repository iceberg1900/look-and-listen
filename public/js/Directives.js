﻿app.directive('myFirstDirect', function () {//function-в даному випадку це фабрика. directive-службове слово.
    return {
        restrict: 'AEC', //обмежуе застосування директиви: А-атрибут Е-елемен С-клас.
        //template: "<h1>Hello</h1>{{data}}=fgfgg={{data2}}",
        templateUrl: 'Templates/MyFirstDirect.html',
        scope: {
            data: "= myData",//це символ, =,@,& - спецсимволи говорять який чином буде привязуватися інфа,&-для привязки колбек функції.!!!!@-значення обекту. &-сам обект.
            data2: '@',
        },
        link: function ($scope, $attrs, $elements) {
            $scope.$watch($scope.file, function() {
                console.log($scope.file);
            });
        }
    };
});