﻿app.controller('PopularsController',
function ($scope, $http) {
    $scope.TemplateUrl = 'Templates/gallery.html';
    $http.get('/api/films/premieres')
    .success(function (films) {
        $scope.films = films;
    });

});