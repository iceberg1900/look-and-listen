﻿app.controller('CartController', function ($scope, $cookieStore, PublicFilmService, $location, PublicOrderService) {
    $scope.TemplateUrl = 'Templates/Cart.html';
    $scope.count = 0;
    $scope.PublicOrder = {};
    var country;
    var cart = $cookieStore.get('cart');
    if (!cart) $scope.PublicOrder.films = [];
    else {
        var ids = [];
        for (var i in cart) ids.push(i);
        PublicFilmService.getAll({ ids: ids, size: ids.length },//this.getAll(filter,success,error);
              function (films) {   //-SUCCESS
                  var tmpCount = 0;
                  var tmpFilms = [];
                  films.items.forEach(function (film) {
                      //tmpFilms[film.id] = {
                      //    film: film,
                      //    count: cart[film.id].count
                      //}
                      //$scope.count += cart[film.id].count
                      tmpFilms.push({
                          film: film,
                          count: cart[film.id].count
                      });
                      tmpCount += cart[film.id].count;
                  });
                  $scope.PublicOrder.films = tmpFilms;
                  $scope.count = tmpCount;
              }, function (reason) {
                  alert('Error'); //->ERROR
                  console.log(reason);
              });
    }
    $scope.$on('CartController:add', function (event, params) {
        //if ($scope.PublicOrder.films[params.film.id]) {
        //    $scope.PublicOrder.films[params.film.id].count +=
        //        params.count;
        //}
        //else {
        //    $scope.PublicOrder.films[params.film.id] = params;
        //}
        var index = $scope.PublicOrder.films.map(function (item) {
            return item.film.id;
        }).indexOf(params.film.id);

        if (index >= 0)
            $scope.PublicOrder.films[params.film.id].count += params.count;
        else
            $scope.PublicOrder.films[params.film.id] = params;

        $scope.count += params.count;

        var cart = $cookieStore.get('cart');
        if (!cart)
            cart = {};//якщо корзини немає то вона створюється
        cart[params.film.id] = {
            id: params.film.id,
            count: $scope.PublicOrder.films[params.film.id].count
        };
        $cookieStore.put('cart', cart);
    });
    $scope.openCart = function () {
        $('#cart-window').modal();
    };
    $scope.completeOrder = function () {
        $location.path('/order');//редірект на іншу сторінку
    };
    $scope.$watch('userPlace', function () {

        if ($scope.userPlace && typeof ($scope.userPlace) === 'object') {
            types = $scope.userPlace.address_components.map(function (item) {
                return item.types;
            });

            countryIndex = types.map(function (item) {
                return item.indexOf('country') >= 0;
            }).indexOf(true);

            cityIndex = types.map(function (item) {
                return item.indexOf('locality') >= 0;
            }).indexOf(true);

            country = countryIndex >= 0 ? $scope.userPlace.address_components[countryIndex].long_name
                        : "";
            city = cityIndex >= 0 ? $scope.userPlace.address_components[cityIndex].long_name
                        : "";

            $scope.PublicOrder.address = {
                address: $scope.userPlace.name,
                city: city,
                country: country,
            };
        }

    });
    $scope.confirmOrder = function () {
        PublicOrderService.add($scope.PublicOrder,
            function (order) {
                $scope.orderId = order.id;
                $('#order-completed').modal();
            }, function (reason) {
                console.log(reason);
            });
    };
});
