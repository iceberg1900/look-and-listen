﻿app.service("PublicFilmService", function ($resource) { //resource -фабрика  ;$http-сервіс, this-вказує на обект самої функції не можливо доступитися щоб змінити юзерам зис дає таку видимість.
    var $provider = $resource("/api/films/:id", { id: '@id' }, {
        update: { method: 'PUT' }
    });

    this.getAll = function (filter, success, error) {
        $provider.get(filter, success, error);
    };

    this.get = function (id, success, error) {
        $provider.get({ id: id }, success, error);//wraper над методом get
        //багато запитів query
        //один запит-get
    };
    
});
//FilmService.getAll()
//FilmService.getAll(1)
//FilmService.add({ title: "title" })
//FilmService.update({ id: 1, title: "title" })!обовязково id 1парметр
//FilmService.remove({ id: 1 })
