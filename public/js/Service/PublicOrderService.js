﻿app.service("PublicOrderService", function ($resource) {
    var $provider = $resource("/api/orders/");
    this.add = function (film, success, error) {
        $provider.save(film, success, error);
    };
});