module.exports = function (grunt) {
    grunt.initConfig({
        jshint: {
            //"curly": true,
            //all: [
            //  'js/**/*.js',
            //  'admin/js/**/*.js'
            //],
            options: {
                curly: true,
                eqeqeq: true,
                eqnull: true,
                browser: true,
                globals: {
                    jQuery: true
                },
            },
            uses_defaults: [
                'js/**/*.js',
                'admin/js/**/*.js'
            ],

            //ignore_warning: {
            //    options: {
            //        '-W015': true,
            //    },
            //    src: [
            //      'js/**/*.js',
            //      'admin/js/**/*.js'
            //    ]
            //}
            //options: {
            //    jshintrc: '.jshintrc'//????? ?????????? ??? ?? ????????????? ? ???? ???? ??? js
            //}
        },
        concat: {
            basic: {//public
                src: ['dist/_bower.js', 'js/App.js', 'js/Service/*js', 'js/controllers/*js'],
                dest: 'dist/basic.js',
            },
            extras: {
                src: ['dist/_bower.js', 'admin/js/App.js', 'admin/js/Service/*js', 'admin/js/directives/*js', 'admin/js/Controllers/*js'],
                dest: 'dist/admin.js',
            },
        },
        uglify: {
            admin_target: {
                files: {
                    'dist/admin.min.js': ['dist/admin.js']
                }
            },
            public_target: {
                files: {
                    'dist/basic.min.js': ['dist/basic.js']
                }
            }
        },
        bower_concat: {
            all: {
                dest: 'dist/_bower.js',
                cssDest: 'dist/_bower.css',
                //exclude: [
                // 'tinymce'                 
                //],
                //bowerOptions: {
                //    relative: false
                //}
                mainFiles: {
                    'jcrop': ['js/jquery.color.js', 'js/jquery.Jcrop.js', 'css/jquery.Jcrop.css']
                }
            }
        },

        sass: {                              // Task
            dist: {                            // Target
                options: {                       // Target options
                    style: 'expanded'
                },
                files: {                         // Dictionary of files
                    'dist/main.css': 'style/Scss.scss',       // 'destination': 'source'
                }
            }
        },
        cssmin: {
            //target: {
            //    files: [{
            //        
            //        src: ['dist/_bower.css','dist/main.css'],
            //        dest: 'dist/res-site.css',
            //        ext: '.min.css'
            //    }]
            //},
            target: {
                files: {
                    'dist/res-site.css': ['dist/_bower.css', 'dist/main.css'],
                }
            }
        }

    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-bower-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');


    // "npm test" runs these tasks
    //grunt.registerTask('test', ['jshint']);//? ?????? ????????? 

    // Default task.
    grunt.registerTask('default', [/*'jshint','sass', 'cssmin',*/ 'concat']);
    grunt.registerTask('prodaction', [/*'jshint',*/'sass', 'cssmin', 'bower_concat', 'concat', 'uglify']);
    grunt.registerTask('bower', [/*'jshint',*/ 'bower_concat']);
};
