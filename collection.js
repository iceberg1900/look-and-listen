/**
 * Created by iryshka on 06.04.2015.
 */
var Users = Bookshelf.Collection.extend({
    model: User
});
var Posts = Bookshelf.Collection.extend({
    model: Post
});
var Categories = Bookshelf.Collection.extend({
    model: Category
});
var Tags = Bookshelf.Collection.extend({
    model: Tag
});