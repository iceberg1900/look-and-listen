var express = require('express');
var repo = require('../data/repository');
var mustbe = require('mustbe').routeHelpers();

module.exports = function (router) {
    router.get('/api/films', function (req, res) {
        var limit = req.param('size') && !isNaN(req.param('size')) ? parseInt(req.param('size')) : 4;
        var page = req.param('page') && !isNaN(req.param('page')) ? parseInt(req.param('page')) : 1;

        var filter = {
            ids: req.param('ids'),
            page: page,
            size: limit
        }

        repo.filmService.getAll(filter).then(function (films) {
            var result = {
                items: films[0],
                total: films[1].get('total'),
                page: page,
                size: limit
            }
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(result));
        });
    });
    router.get('/api/films/premieres', function (req, res) {
        repo.filmService.getPremiere().then(function (films) {
            res.setHeader('Content-Type', 'application/json');
            var data = [];
            films.forEach(function (film) {
                data.push({
                    id: film.get('id'),
                    title: film.get('title'),
                    fullimage: film.get('poster'),
                    //fullimage: film.get('fullimage.url'),
                    premiere: film.get('premiere')
                });
            });
            res.setHeader('Content-Type', 'application/json');
            res.json(films);
        });
    });
    router.get('/api/films/:id', function (req, res) {
        var id = req.param('id');
        repo.filmService.get(id).then(function (film) {
            res.setHeader('Content-Type', 'application/json');
            if (film) res.end(JSON.stringify(film));
            else res.status(404).end(JSON.stringify({ message: "Film not found." }));
        });
    });
    router.get('/api/films/alias/:alias', function (req, res) {
        var alias = req.param('alias');
        repo.filmService.getByAlias(alias).then(function (film) {
            res.setHeader('Content-Type', 'application/json');
            if (film) res.end(JSON.stringify(film));
            else res.status(404).end(JSON.stringify({ message: "Film not found." }));
        });
    });
    router.post('/api/films', mustbe.authorized("admin", function (req, res) {
        var model = req.body;
        repo.filmService.add(model).then(function (film) {
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(film));
        });
    }));

    router.put('/api/films/:id', mustbe.authorized("admin", function (req, res) {//order instead of film
        var id = req.param('id');
        var model = req.body;
        repo.filmService.update(id, model).then(function (film) {
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(film));
        });
    }));
    router.delete('/api/films/:id', mustbe.authorized("admin", function (req, res) {
        var id = req.param('id');
        repo.filmService.remove(id);
        res.end();
    }));

};