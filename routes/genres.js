var express = require('express');
var repo = require('../data/repository');

module.exports = function (router) {
    router.get('/api/genres', function (req, res) {
        repo.genreService.getAll(req.query.searchTerm).then(function (genres) {
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(genres));
        });
    });
};