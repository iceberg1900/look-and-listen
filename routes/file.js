﻿var express = require('express');
var formidable = require('formidable');
var repo = require('../data/repository');
var fs = require('fs');

var dbox = require("dbox"),
    stdin = process.stdin,
    stdout = process.stdout,
    app_key = 'eg9pevxifnpygmv',
    app_secret = 'vaolq4k804ycwbw',
        client = {};

var app = dbox.app(
    {
        'app_key': app_key,
        'app_secret': app_secret,
        'root': 'sandbox'
    });
var access = {};

module.exports = function (router) {
    router.post('/api/file/upload', function (req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (error, fields, file) {
            fs.readFile(file.image.path, function (err, data) {
                if (err) throw err;
                var filename = fileName();
                client.put(filename, data, function (status, reply) {
                    repo.mediaService.add(filename).then(function (media) {
                        res.setHeader('Content-Type', 'application/json');
                        res.end(JSON.stringify(media));
                    });
                });
            });
        });
    });
    router.get('/api/file/:path', function (req, res) {
        var path = req.params.path;
        var stream = client.stream(path);
        res.redirect(stream.uri.href);
    });
    router.get('/api/thumbnails/:path', function (req, res) {
        var path = req.params.path;
        client.thumbnails(path, function (status, reply) {
            res.setHeader('Content-Type', 'image/png');
            res.send(reply);
        })
    });
};


app.requesttoken(function (status, request_token) {
    if (request_token) {
        console.log('Please visit to authorize your app:');
        console.log(request_token.authorize_url);
        ask('Is this done? (y)', /^y$/, function (answer) {
            app.accesstoken(request_token, function (status, access_token) {
                console.log('app_key: ' + app_key);
                console.log('app_secret: ' + app_secret);
                console.log('oauth_token: ' + access_token.oauth_token);
                console.log('oauth_token_secret: ' + access_token.oauth_token_secret);
                console.log('uid: ' + access_token.uid);
                access = access_token;
                client = app.client(access_token);
            });
        });
    }
});

function ask(question, format, callback) {
    stdin.resume();
    stdout.write(question + ": ");

    stdin.once('data', function (data) {
        data = data.toString().trim();

        if (format.test(data)) {
            callback(data);
        } else {
            stdout.write("It should match: " + format + "\n");
            ask(question, format, callback);
        }
    });
}

function fileName() {
    var d = new Date().getTime();
    var guid = 'look-and-listen-xxxx-xxxx-xxxx.png'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    return guid;
};