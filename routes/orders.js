﻿var express = require('express');
var repo = require('../data/repository');
var mustbe = require('mustbe').routeHelpers();

module.exports = function (router) {
    router.post('/api/orders', function (req, res) {
        var model = req.body;
        repo.orderService.add(model).then(function (order) {
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(order));
        });
    });
    router.get('/api/orders', mustbe.authorized("authorize", function (req, res) {
        var limit = req.param('size') && !isNaN(req.param('size')) ? parseInt(req.param('size')) : 20;
        var page = req.param('page') && !isNaN(req.param('page')) ? parseInt(req.param('page')) : 1;
        var status = req.param('status');
        var filter = {
            status: status,
            page: page,
            size: limit
        }
        repo.orderService.getAll(filter).then(function (orders) {
            var result = {
                items: orders[0],
                total: orders[1].get('total'),
                page: page,
                size: limit,
                status: filter.status
            }
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(result));
        });
    }))
    router.get('/api/orders/:id', function (req, res) {
        var id = req.param('id');
        repo.orderService.get(id).then(function (order) {
            res.setHeader('Content-Type', 'application/json');
            if (order) res.end(JSON.stringify(order));
            else res.status(404).end(JSON.stringify({ message: "Order not found." }));
        });
    });
    router.put('/api/orders/:id', mustbe.authorized("authorize", function (req, res) {//order instead of film - changed
        var id = req.param('id');
        var model = req.body;
        repo.orderService.updateStatus(id, model.status).then(function (film) {
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(film));
        });
    }));
};