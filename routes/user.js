﻿var mustbe = require('mustbe').routeHelpers();
var express = require('express');
var users = require('../user-config');
module.exports = function (router) {
    router.get('/api/user', mustbe.authorized("authorize", function (req, res) {
        res.json(req.session.user);
    }));
    router.post('/api/user/login', function (req, res) {
     

        var login = req.param('login');
        var password = req.param('pass');
        var userIndex = users.list
                            .map(function (user) { return user.login === login && user.pass === password; })
                            .indexOf(true);
        if (userIndex >= 0) {
            var user = ({
                name: login,
                role: users.list[userIndex].role
            });
            req.session.user = user;
            res.json(user);
        }
        else {
            res.status(401);
            res.end();
        }
    });
    router.post('/api/user/logout', function (req, res) {
        delete req.session.user;
        res.clearCookie("express:sess");
        res.clearCookie("express:sess.id");
        res.end();
    })
};
