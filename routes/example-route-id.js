/**
 * Created by iryshka on 06.04.2015.
 */
router.route('/posts/category/:id')
    .get(function (req, res) {
        Category.forge({id: req.params.id})
            .fetch({withRelated: ['posts']})
            .then(function (category) {
                var posts = category.related('posts');
                res.json({error: false, data: posts.toJSON()});
            })
            .otherwise(function (err) {
                res.status(500).json({error: true, data: {message: err.message}});
            });
    });
router.route('/posts/tag/:slug')
    .get(function (req, res) {
        Tag.forge({slug: req.params.slug})
            .fetch({withRelated: ['posts']})
            .then(function (tag) {
                var posts = tag.related('posts');
                res.json({error: false, data: posts.toJSON()});
            })
            .otherwise(function (err) {
                res.status(500).json({error: true, data: {message: err.message}});
            });
    });