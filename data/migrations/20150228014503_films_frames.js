'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('films_frames', function (t) {
        t.integer('film_id').unsigned().index().references('films.id');
        t.integer('media_id').unsigned().index().references('medias.id');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('films_frames');
};
