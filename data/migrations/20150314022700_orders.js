'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('orders', function (t) {
        t.increments('id').primary();
        t.integer('address_id').unsigned().index().references('address.id');
        t.integer('user_id').unsigned().index().references('users.id');
        t.integer('status');
        t.string('phone', 20);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('orders');
};
