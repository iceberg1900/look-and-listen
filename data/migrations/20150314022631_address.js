'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('address', function (t) {
        t.increments('id').primary();
        t.string('address', 100);
        t.string('country', 100);
        t.string('city', 100);
        t.string('postcode', 100);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('address');
};
