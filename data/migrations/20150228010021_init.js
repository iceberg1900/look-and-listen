'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('films', function (t) {
        t.increments('id').primary();
        t.string('title', 100);
        t.text('description');
        t.integer('year');
        t.integer('duration');
        t.date('premiere')
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('films');
};