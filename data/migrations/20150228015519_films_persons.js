'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('films_stars', function (t) {
        t.integer('film_id').unsigned().index().references('films.id');
        t.integer('person_id').unsigned().index().references('persons.id');
    }).createTable('films_authors', function (t) {
        t.integer('film_id').unsigned().index().references('films.id');
        t.integer('person_id').unsigned().index().references('persons.id');
    }).createTable('films_directors', function (t) {
        t.integer('film_id').unsigned().index().references('films.id');
        t.integer('person_id').unsigned().index().references('persons.id');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('films_stars')
        .dropTable('films_authors')
        .dropTable('films_directors')
};
