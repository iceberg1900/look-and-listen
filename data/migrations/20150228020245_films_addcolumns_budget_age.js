'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.table('films', function (table) {
        table.integer('age');
        table.decimal('budget');
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('films', function (table) {
        t.dropColumn('age');
        t.dropColumn('budget');
    });
};
