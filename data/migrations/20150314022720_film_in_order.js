'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('film_in_order', function (t) {
        t.integer('film_id').unsigned().index().references('films.id');
        t.integer('order_id').unsigned().index().references('orders.id');
        t.integer('count');
        t.decimal('price', 8, 2)
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('film_in_order');
};
