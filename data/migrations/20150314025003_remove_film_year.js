'use strict';

exports.up = function (knex, Promise) {
    return knex.schema.table('films', function (table) {
        table.dropColumn('year');
    })
  
};

exports.down = function(knex, Promise) {
    return knex.schema.table('films', function (table) {
        table.integer('year')
    })
};
