'use strict';

exports.up = function (knex, Promise) {
    return knex.schema.table('films', function (table) {
        table.dropColumn('budget');
        table.string('alias');
        table.decimal('price', 8, 2)
    })
};

exports.down = function (knex, Promise) {
    return knex.schema.table('films', function (table) {
        table.dropColumn('alias');
        table.dropColumn('price');
        table.decimal('budget')
    });
};

