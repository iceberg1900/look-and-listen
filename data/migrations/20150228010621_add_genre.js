'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('genres', function (t) {
        t.increments('id').primary();
        t.string('name', 100);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('genres');
};
