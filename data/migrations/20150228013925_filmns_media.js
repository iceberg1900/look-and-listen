'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.table('films', function (table) {
        table.integer('poster_id').nullable().unsigned().index().references('medias.id').onDelete('cascade');;
        table.integer('fullImage_Id').nullable().unsigned().index().references('medias.id').onDelete('cascade');;
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.table('films', function (table) {
        t.dropForeign('poster_id');
        t.dropColumn('poster_id');
        t.dropForeign('fullImage_id');
        t.dropColumn('fullImage_id');
    })
};
