'use strict';

exports.up = function (knex, Promise) {
    return knex.schema.table('films', function (table) {
        table.decimal('budget', 8, 2)
    })
};

exports.down = function (knex, Promise) {
    return knex.schema.table('films', function (table) {
        table.dropColumn('budget');
    });
};