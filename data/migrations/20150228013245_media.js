'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('medias', function (t) {
        t.increments('id').primary();
        t.string('url', 100);
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('medias');
};
