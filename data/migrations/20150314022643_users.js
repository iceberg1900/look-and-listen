'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('users', function (t) {
        t.increments('id').primary();
        t.string('token', 64);
        t.string('oath_name', 20);
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('users');
};
