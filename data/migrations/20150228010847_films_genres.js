'use strict';

exports.up = function(knex, Promise) {
    return knex.schema.createTable('films_genres', function (t) {
        t.integer('film_id').unsigned().index().references('films.id');
        t.integer('genre_id').unsigned().index().references('genres.id');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('films_genres');
};
