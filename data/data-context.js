﻿var dbConfig = {
    client: 'mysql',
    connection: {
        host: 'localhost',
        user: 'user',
        password: 'password',
        database: 'look_and_listen',
        charset: 'utf8'
    },
    migrations: {
        tableName: 'migrations'
    }
};
var knex = require('knex')(dbConfig);

module.exports = knex;