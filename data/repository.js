﻿var filmService = require('./services/film-service.js');
var genreService = require('./services/genre-service.js');
var mediaService = require('./services/media-service.js');
var orderService = require('./services/order-service.js');
var repository = {
    filmService: filmService,
    genreService: genreService,
    mediaService: mediaService,
    orderService: orderService
};
module.exports = repository;