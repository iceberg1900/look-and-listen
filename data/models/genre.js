﻿var context = require('../data-context.js');
var bookshelf = require('bookshelf')(context);
var Genre = bookshelf.Model.extend({
    tableName: 'genres'
});

module.exports = Genre;

