﻿var context = require('../data-context.js');
var bookshelf = require('bookshelf')(context);

var Genre = require('./genre.js');
var Media = require('./media.js');
var Person = require('./person.js');

var Film = bookshelf.Model.extend({
    tableName: 'films',
    //genres: function () {
    //    return this.belongsToMany(Genre, 'films_genres', 'film_id', 'id');
    //},
    poster: function () {
        return this.belongsTo(Media, 'poster_id');
    },
    fullimage: function () {
        return this.belongsTo(Media, 'fullImage_Id');
    },
    frames: function () {
        return this.belongsToMany(Media, 'films_frames', 'film_id', 'media_id');
    },
    //stars: function () {
    //    return this.belongsToMany(Person, 'films_stars', 'filmid', 'personid')
    //},
    //authors: function () {
    //    return this.belongsToMany(Person)
    //},
    //directors: function () {
    //    return this.belongsToMany(Person)
    //}
});

module.exports = Film;

