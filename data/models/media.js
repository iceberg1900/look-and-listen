﻿var context = require('../data-context.js');
var bookshelf = require('bookshelf')(context);
var Media = bookshelf.Model.extend({
    tableName: 'medias'
});

module.exports = Media;

