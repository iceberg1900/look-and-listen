﻿var context = require('../data-context.js');
var bookshelf = require('bookshelf')(context);
var Person = bookshelf.Model.extend({
    tableName: 'persons'
});

module.exports = Person;

