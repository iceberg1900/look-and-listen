﻿var context = require('../data-context.js');
var bookshelf = require('bookshelf')(context);


var User = bookshelf.Model.extend({
    tableName: 'users',
});

module.exports = User;

