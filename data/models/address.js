﻿var context = require('../data-context.js');
var bookshelf = require('bookshelf')(context);
var Address = bookshelf.Model.extend({
    tableName: 'address',
});

module.exports = Address;

