﻿var context = require('../data-context.js');
var bookshelf = require('bookshelf')(context);

var Address = require('./address.js');
var Users = require('./user.js');
var OrderFilm = require('./order-film.js');

var Order = bookshelf.Model.extend({
    tableName: 'orders',
    address: function () {
        return this.belongsTo(Address, 'address_id');
    },
    user: function () {
        return this.belongsTo(Users, 'user_id');
    },
    filmsProjection: function () {
        return this.hasMany(OrderFilm, 'order_id');
    }
});

module.exports = Order;

