﻿var context = require('../data-context.js');
var bookshelf = require('bookshelf')(context);
var Films = require('./film.js');

var OrderFilm = bookshelf.Model.extend({
    tableName: 'film_in_order',
    film: function () {
        return this.belongsTo(Films, 'film_id');
    },
});

module.exports = OrderFilm;