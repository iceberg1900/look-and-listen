﻿var Genre = require('../models/genre.js');
var genreService = {
    getAll: function (query) {
        var res =  new Genre();
		if(query && query.trim()){
			res = res.where('name', 'LIKE', query+'%');
		}
        return res.fetchAll();
    },
};
module.exports = genreService;