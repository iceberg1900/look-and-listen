﻿var Order = require('../models/order.js');
var Address = require('../models/address.js');
var OrderFilm = require('../models/order-film.js');
var Promise = require('bluebird');

var orderService = {
    get: function (id) {
        return new Order({ id: id }).fetch({ withRelated: ['address', 'user', 'filmsProjection', 'filmsProjection.film'] });
    },
    getAll: function () {
        return new Order().fetchAll({ withRelated: ['address', 'user', 'filmsProjection.film'] });
    },

    getAll: function (filter) {
        var fquery = function (qb) {
            if (filter && filter.status)
                qb.where('status', '=', filter.status);
        };
        var order = new Order();
        var offset = parseInt(filter.page) && !isNaN(filter.page)
           ? Math.abs(parseInt(filter.page) - 1) * filter.size
           : 0;
        return Promise.all([
                        order.query(function (qb) {
                            fquery(qb);
                            qb.limit(filter.size);
                            qb.offset(offset);
                        }).fetchAll({ withRelated: ['address', 'user', 'filmsProjection.film'] }),

                        order.query(function (qb) {
                            fquery(qb);
                            qb.count('* as total');
                        }).fetch()
        ])
    },

    updateStatus: function (id, newstatus) {
        var order = new Order({ id: id });
        return order.save({
            status: newstatus
        });
    },
        
    add: function (order) {
        var films = order.films ? order.films.map(function (item) {
            return new OrderFilm({
                film_id: item.film.id,
                count: item.count,
                price: item.film.price
            })
        }) : undefined;
        var address = order.address ? new Address({
            address: order.address.address,
            country: order.address.country,
            city: order.address.city,
            postcode: order.address.postcode
        }) : undefined;
        var newOrder = new Order({
            status: 0,
            phone: order.phone,
        });
        var promise = newOrder.save();
        promise.then(function (order) {
            if (address)
                address.save().then(function (addr) {
                    order.set('address_id', addr.id).save();
                });
            if (films)
                films.forEach(function (film) {
                    console.log("film", film)
                    film.set('order_id', order.id).save();
                })
        })
        return promise;
    },
    remove: function (id) {
        new Order({ id: id }).destroy();
    }
};
module.exports = orderService;