﻿var Film = require('../models/film.js');
var Media = require('../models/media.js');
var Promise = require('bluebird');
var filmService = {
    get: function (id) {
        return new Film({ id: id }).fetch({ withRelated: ['poster', 'frames', 'fullimage'] });
    },
    getByAlias: function (alias) {
        console.log(alias)
        return new Film({ alias: alias }).fetch({ withRelated: ['poster', 'frames', 'fullimage'] });
    },
    getPremiere: function () {
        var today = new Date();

        return new Film()
        .query(function (qb) {
            qb.limit(5);
            qb.where('premiere', '>', today);
        })
            .fetchAll({ withRelated: ['fullimage'] });
    },
    getAll: function (filter) {
        var fquery = function (qb) {
            if (filter && filter.ids)
                qb.where('films.id', 'in', filter.ids);
        };

        var film = new Film();
        //var limit = filter.size && !isNaN(filter.size) ? parseInt(filter.size) : 12;
        var offset = parseInt(filter.page) && !isNaN(filter.page)
            ? Math.abs(parseInt(filter.page) - 1) * filter.size
            : 0;
        //qb.limit(limit);
        //qb.offset(offset);

        return Promise.all([
                film.query(function (qb) {
                    fquery(qb);
                    qb.limit(filter.size);
                    qb.offset(offset);
                }).fetchAll({ withRelated: ['poster', 'frames', 'fullimage'] }),

                film.query(function (qb) {
                    fquery(qb);
                    qb.count('* as total');
                }).fetch()
        ])
    },
    add: function (film) {
        var newFilm = new Film({
            title: film.title,
            description: film.description,
            duration: film.duration,
            premiere: film.premiere,
            age: film.age,
            budget: film.budget,
            alias: film.alias,
            price: film.price
        });
        if (film.poster && film.poster.id) newFilm.set('poster_id', film.poster.id);
        if (film.fullimage && film.fullimage.id) newFilm.set('fullImage_Id', film.fullimage.id);
        return newFilm.save();
    },
    update: function (id, film) {
        var newFilm = new Film({ id: id });
        return newFilm.save({
            title: film.title,
            description: film.description,
            duration: film.duration,
            premiere: film.premiere,
            age: film.age,
            budget: film.budget,
            alias: film.alias,
            price: film.price,
            poster_id: film.poster && film.poster.id ? film.poster.id : null,
            fullImage_Id: film.fullimage && film.fullimage.id ? film.fullimage.id : null,
        }, { patch: true });
    },
    remove: function (id) {
        new Film({ id: id }).destroy();
    }
};
module.exports = filmService;

//var today = new Date();
//var dd = today.getDate();
//var mm = today.getMonth()+1; //January is 0!
//var yyyy = today.getFullYear();

//if(dd<10) {
//    dd='0'+dd
//} 

//if(mm<10) {
//    mm='0'+mm
//} 

//today = mm+'/'+dd+'/'+yyyy;
//document.write(today);