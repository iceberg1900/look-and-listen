﻿var Media = require('../models/media.js');
var mediaService = {
    add: function (url) {
        return new Media({
            url: url
        }).save();
    },
};
module.exports = mediaService;