/**
 * Created by iryshka on 06.04.2015.
 */
//added from http://blog.ragingflame.co.za/2014/12/16/building-a-simple-api-with-express-and-bookshelfjs
var _ = require('lodash');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
// application routing
var router = express.Router();
// body-parser middleware for handling request variables
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());