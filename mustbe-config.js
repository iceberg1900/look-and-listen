﻿var mustBe = require("mustbe");
module.exports = function (config) {

    config.routeHelpers(function (rh) {
        // get the current user from the request object
        rh.getUser(function (req, cb) {
            cb(null, req.session.user);
        });

        // what do we do when the user is not authorized?
        rh.notAuthorized(function (req, res, next) {
            res.status(401).end();
        });
    });

    config.activities(function (activities) {
        activities.can("admin", function (identity, params, cb) {
            cb(null, identity.user && identity.user.role === "admin");
        });
        activities.can("operator", function (identity, params, cb) {
            cb(null, identity.user && identity.user.role === "operator");
        });
        activities.can("moderator", function (identity, params, cb) {
            cb(null, identity.user && identity.user.role === "moderator");
        });
        activities.can("authorize", function (identity, params, cb) {
            cb(null, !!identity.user);
        });
    });

};
