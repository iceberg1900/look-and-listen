var express = require('express');
var session = require('cookie-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('cookie-session');
var app = express();
app.use(session({
    keys: ['temp']
}));
//Route View
app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
//Route View

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({keys:["temp"]}));

//must be
var mustBe = require("mustbe");
var mustBeConfig = require("./mustbe-config");
mustBe.configure(mustBeConfig);

module.exports = app;
console.log('Server started!');


//Route Map
var router = express.Router();
require('./routes/films')(router);
require('./routes/genres')(router);
require('./routes/file')(router);
require('./routes/orders')(router);
require('./routes/user')(router);
app.use('/', router);

//Route Map

//app.use(function (req, res, next) {
//    var err = new Error('Not Found');
//    err.status = 404;
//    next(err);
//});

app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    console.log("in app.js error=  " + err);
    res.writeHead(err.status || 500,
        {
            'Content-Type': 'application/json'
        });
    var reason = {
        message: err.toString()
};
    res.end(JSON.stringify(reason));
});


//must be
var mustBe = require("mustbe");
var mustBeConfig = require("./mustbe-config");
mustBe.configure(mustBeConfig);

module.exports = app;
console.log('Server started!');
